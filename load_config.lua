local config_file = io.open(minetest.get_modpath("hub_manager") .. "/config.txt", "r")

assert(config_file ~= nil, "CAN'T LOAD HUB_MANAGER, config.txt IS MISSING!")

local data = string.split(config_file:read("*all"), "\n")

local hub_spawn_point = minetest.string_to_pos(string.match(data[1], "= (.*)"))
local additional_items = {}
local physics = {}

-- oggetti
for i = 3, 7 do
  additional_items[i-2] = string.match(data[i], "=(.*)")
end

-- fisica
for i = 9, 11 do
  physics[string.match(data[i], "(.*)=")] = tonumber(string.match(data[i], "=(.*)"))
end

local sneak = string.match(data[12], "=(.*)")
local sneak_glitch = string.match(data[13], "=(.*)")
local new_move = string.match(data[14], "=(.*)")

if sneak == "true" then
  physics.sneak = true
else
  physics.sneak = false
end

if sneak_glitch == "true" then
  physics.sneak_glitch = true
else
  physics.sneak_glitch = false
end

if new_move == "true" then
  physics.new_move = true
else
  physics.new_move = false
end

config_file:close()




function hub_manager.apply_hub_physics(player)
  player:set_physics_override(physics)
end



----------------------------------------------
-----------------GETTERS----------------------
----------------------------------------------

function hub_manager.get_hub_spawn_point()
  return hub_spawn_point
end



function hub_manager.get_additional_items()
  return additional_items
end





----------------------------------------------
-----------------SETTERS----------------------
----------------------------------------------

function hub_manager.set_hub_physics(player)
  player:set_physics_override(physics)
end
