hub_manager = {}



function hub_manager.set_items(player)

  local inv = player:get_inventory()

  local hotbar_items = {
    nil,
    nil,
    nil,
    nil,
    nil,
    "hub_manager:profile",
    "hub_manager:achievements",
    "hub_manager:settings"
  }

  local additional_items = hub_manager.get_additional_items()

  -- eventuali oggetti aggiuntivi
  for i = 1, 5 do
    hotbar_items[i] = additional_items[i]
  end

  inv:set_list("main", hotbar_items)
end
