# Hub Manager

Easily manage your mini-games on Minetest  

<a href="https://liberapay.com/Zughy/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support my work"/></a>


## Customisation
Check config.txt to change spawn point, to add more items in the hotbar or to change physics values


---

Images are under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
