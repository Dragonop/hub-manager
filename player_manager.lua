minetest.register_on_joinplayer(function(player)

  player:set_pos(hub_manager.get_hub_spawn_point())
  player:get_inventory():set_list("main", {})
  player:get_inventory():set_list("craft", {})

  hub_manager.set_items(player)
  hub_manager.set_hub_physics(player)

end)



minetest.register_on_player_hpchange(function(player, hp_change, reason)

  minetest.chat_send_player("singleplayer", reason.type)

  -- se non è in arena, disabilito danno da caduta e PvP
  if not arena_lib.is_player_in_arena(player:get_player_name()) and ( reason.type == "fall" or reason.type == "punch" ) then
    return 0
  end

  return hp_change

end, true)
