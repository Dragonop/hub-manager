local S = minetest.get_translator("hub_manager")



minetest.register_tool("hub_manager:profile", {

  description = S("Profile"),
  inventory_image = "hubmanager_profile.png",
  groups = {not_in_creative_inventory = 1, oddly_breakable_by_hand = "2"},
  on_place = function() end,
  on_drop = function() end,

  on_use = function(itemstack, user, pointed_thing)
    minetest.chat_send_player(user:get_player_name(), "Coming soon")
  end

})



minetest.register_tool("hub_manager:achievements", {

  description = S("Achievements"),
  inventory_image = "hubmanager_achievements.png",
  groups = {not_in_creative_inventory = 1, oddly_breakable_by_hand = "2"},
  on_place = function() end,
  on_drop = function() end,

  on_use = function(itemstack, user, pointed_thing)
    minetest.chat_send_player(user:get_player_name(), "Coming soon")
  end

})



minetest.register_tool("hub_manager:settings", {

  description = S("Settings"),
  inventory_image = "hubmanager_settings.png",
  groups = {not_in_creative_inventory = 1, oddly_breakable_by_hand = "2"},
  on_place = function() end,
  on_drop = function() end,

  on_use = function(itemstack, user, pointed_thing)
    minetest.chat_send_player(user:get_player_name(), "Coming soon")
  end

})
