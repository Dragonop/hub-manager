local version = "0.1.0-dev"

dofile(minetest.get_modpath("hub_manager") .. "/api.lua")
dofile(minetest.get_modpath("hub_manager") .. "/items.lua")
dofile(minetest.get_modpath("hub_manager") .. "/load_config.lua")
dofile(minetest.get_modpath("hub_manager") .. "/player_manager.lua")

minetest.log("action", "[HUB_MANAGER] Mod initialised, running version " .. version)
